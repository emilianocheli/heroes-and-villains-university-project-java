import java.util.Collections;
import java.util.Vector;

import Criterio.Criterio;
import EntidadAbstracta.EntidadAbstracta;

public  class Juego implements Cloneable {
   //Atributos Juego
	
	private  Vector<EntidadAbstracta> personajes;
   //Contructor Juego
	
		public Juego() {
			personajes = new Vector<EntidadAbstracta>();
		}		
   //Metotodos Juego
		
		public void addPersonaje(EntidadAbstracta e) {
			personajes.add(e);
		}		
		public EntidadAbstracta pelea(EntidadAbstracta p1, EntidadAbstracta p2, Criterio c) {
			
			if (c.compare(p1, p2)==-1)
	         return p2;//GANA
		 	else
				if (c.compare(p1, p2)==1)   
					return p1;//GANA
				else
                return null;
		}
		public Vector <EntidadAbstracta> buscarVencedores( Vencedor c) {
			Vector <EntidadAbstracta> res = new Vector <EntidadAbstracta>();
			for (int i = 0; i < personajes.size(); i++) 
			{	
				Object a1=personajes.elementAt(i);
				if (c.compareTo(a1)==-1) 
				res.add(personajes.elementAt(i));
			}
			return res;
		}
		
		  public Object clone(){
			  Juego obj=null;
		        try{
		            obj=(Juego)super.clone();
		        }catch(CloneNotSupportedException ex){
		            System.out.println(" no se puede duplicar");
		        }	   
		        return obj;
		    }

		public Vector <EntidadAbstracta> listarOrdenado(Criterio may) {
			@SuppressWarnings("unchecked")
			Vector<EntidadAbstracta> resultado = (Vector<EntidadAbstracta>)personajes.clone();
			Collections.sort(resultado, may);	
			return resultado;
		}
		
}
