import EntidadAbstracta.EntidadAbstracta;


public class Vencedor implements Comparable<Object> {
	private String carac;
	private double valor;
	
	public Vencedor( String carac,double valor){
		this.carac=carac;
		this.valor=valor;	
	}
	public int compareTo(Object o) {
		EntidadAbstracta e1=(EntidadAbstracta)o;
		if (this.valor<e1.getCaracteristica(carac))
		return -1;
		else 
			return 0;
	}


}
