import java.util.Vector;

import Calculadores.CalculadorMax;
import Calculadores.CalculadorMin;
import Calculadores.CalculadorPromedio;
import Calculadores.CalculadorSuma;
import Caracteristica.Caracteristica;
import Caracteristica.CaracteristicaCondicional;
import Caracteristica.CaracteristicaDivision;
import Caracteristica.CaracteristicaFija;
import Criterio.CMayor;
import Criterio.CMenor;
import Criterio.Criterio;
import EntidadAbstracta.EntidadAbstracta;
import EntidadAbstracta.Liga;
import EntidadAbstracta.Personaje;

public class Main {

		public static void main(String[] args) {
			
	        //============================= Crear Personajes==============================*/
		
			Personaje batman=new Personaje("Batman");
			Personaje superman=new Personaje("Superman");
			Personaje robin=new Personaje("Robin");
			Personaje flash=new Personaje("Flash");
			Personaje capitanfrio=new Personaje("Capitan Frio");
			Personaje guason=new Personaje("Guason");
			Personaje lexluthor=new Personaje("Lex Luthor");
			Personaje gatubela=new Personaje("Gatubela");
			Personaje goku=new Personaje("Goku");
			
			                     /*--por fuerza--*/
			
			superman.setCaracteristica("Fuerza", new CaracteristicaFija(2600));
			batman.setCaracteristica("Fuerza", new CaracteristicaFija(950));			
			robin.setCaracteristica("Fuerza", new CaracteristicaFija(710));
			flash.setCaracteristica("Fuerza", new CaracteristicaFija(900));
			capitanfrio.setCaracteristica("Fuerza", new CaracteristicaFija(600));			
			guason.setCaracteristica("Fuerza", new CaracteristicaFija(0));
			lexluthor.setCaracteristica("Fuerza", new CaracteristicaFija(1000));
			gatubela.setCaracteristica("Fuerza", new CaracteristicaFija(720));
			goku.setCaracteristica("Fuerza", new CaracteristicaFija(720));
			
			                     /*--por Velocidad--*/
			
			lexluthor.setCaracteristica("Velocidad", new CaracteristicaFija(70));
			superman.setCaracteristica("Velocidad", new CaracteristicaFija(1200));
			batman.setCaracteristica("Velocidad", new CaracteristicaFija(0));			
			robin.setCaracteristica("Velocidad", new CaracteristicaFija(60));
			flash.setCaracteristica("Velocidad", new CaracteristicaFija(151));
			capitanfrio.setCaracteristica("Velocidad", new CaracteristicaFija(150));			
			guason.setCaracteristica("Velocidad", new CaracteristicaFija(60));
			lexluthor.setCaracteristica("Velocidad", new CaracteristicaFija(70));
			gatubela.setCaracteristica("Velocidad", new CaracteristicaFija(90));
			goku.setCaracteristica("Velocidad", new CaracteristicaFija(100));
			
			                       /*--por peso--*/
			
			superman.setCaracteristica("Peso", new CaracteristicaFija(107));
			batman.setCaracteristica("Peso", new CaracteristicaFija(96));			
			robin.setCaracteristica("Peso", new CaracteristicaFija(65));
			flash.setCaracteristica("Peso", new CaracteristicaFija(90));
			capitanfrio.setCaracteristica("Peso", new CaracteristicaFija(0));			
			guason.setCaracteristica("Peso", new CaracteristicaFija(72));
			lexluthor.setCaracteristica("Peso", new CaracteristicaFija(95));
			gatubela.setCaracteristica("Peso", new CaracteristicaFija(0));
			goku.setCaracteristica("Peso", new CaracteristicaFija(0));
			
			
			                    /*--por altura--*/
			
			superman.setCaracteristica("Altura", new CaracteristicaFija(1.92));
			batman.setCaracteristica("Altura", new CaracteristicaFija(1.89));			
			robin.setCaracteristica("Altura", new CaracteristicaFija(1.75));
			flash.setCaracteristica("Altura", new CaracteristicaFija(0));
			capitanfrio.setCaracteristica("Altura", new CaracteristicaFija(1.88));			
			guason.setCaracteristica("Altura", new CaracteristicaFija(1.85));
			lexluthor.setCaracteristica("Altura", new CaracteristicaFija(1.88));
			gatubela.setCaracteristica("Altura", new CaracteristicaFija(1.69));
			goku.setCaracteristica("Altura", new CaracteristicaFija(1.88));
			
			                  /*--por inteligencia--*/
			
			superman.setCaracteristica("inteligencia", new CaracteristicaFija(0));
			batman.setCaracteristica("inteligencia", new CaracteristicaFija(150));			
			robin.setCaracteristica("inteligencia", new CaracteristicaFija(100));
			flash.setCaracteristica("inteligencia", new CaracteristicaFija(50));
			capitanfrio.setCaracteristica("inteligencia", new CaracteristicaFija(0));			
			guason.setCaracteristica("inteligencia", new CaracteristicaFija(150));
			lexluthor.setCaracteristica("inteligencia", new CaracteristicaFija(0));
			gatubela.setCaracteristica("inteligencia", new CaracteristicaFija(150));
			goku.setCaracteristica("inteligencia", new CaracteristicaFija(60));
			
			                      /*--por maldad--*/
			
			superman.setCaracteristica("maldad", new CaracteristicaFija(0));
			batman.setCaracteristica("maldad", new CaracteristicaFija(0));			
			robin.setCaracteristica("maldad", new CaracteristicaFija(0));
			flash.setCaracteristica("maldad", new CaracteristicaFija(0));
			capitanfrio.setCaracteristica("maldad", new CaracteristicaFija(50));			
			guason.setCaracteristica("maldad", new CaracteristicaFija(100));
			lexluthor.setCaracteristica("maldad", new CaracteristicaFija(75));
			gatubela.setCaracteristica("maldad", new CaracteristicaFija(25));
			goku.setCaracteristica("maldad", new CaracteristicaFija(0));
			
			
		   //=============================== Conducta de un personaje===============================*/
				
			                              /*=========Resistencia al fuego===========*/
			
			/*superman*/
		    Caracteristica si=new CaracteristicaFija(superman.getCaracteristica("Velocidad"));
	        Caracteristica valore=new CaracteristicaFija(100);
	        Caracteristica entonces=new CaracteristicaFija(superman.getCaracteristica("Velocidad"));
	        Caracteristica sino=new CaracteristicaFija(1);		
			superman.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si,valore,entonces,sino));	
			
			/*flash*/
	        Caracteristica si2=new CaracteristicaFija((flash.getCaracteristica("Velocidad")));
            Caracteristica valore2=new CaracteristicaFija(100);
            Caracteristica entonces2=new CaracteristicaFija(flash.getCaracteristica("Velocidad"));
            Caracteristica sino2=new CaracteristicaFija(1);		
			flash.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si2,valore2,entonces2,sino2));
			
			/*capitanfrio*/
		    Caracteristica si3=new CaracteristicaFija((capitanfrio.getCaracteristica("Velocidad")));
	        Caracteristica valore3=new CaracteristicaFija(100);
	        Caracteristica entonces3=new CaracteristicaFija(capitanfrio.getCaracteristica("Velocidad"));
	        Caracteristica sino3=new CaracteristicaFija(1);	
			capitanfrio.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si3,valore3,entonces3,sino3));
			
			/*batman*/
		    Caracteristica si4=new CaracteristicaFija((batman.getCaracteristica("Velocidad")));
            Caracteristica valore4=new CaracteristicaFija(100);
            Caracteristica entonces4=new CaracteristicaFija(batman.getCaracteristica("Velocidad"));
            Caracteristica sino4=new CaracteristicaFija(1);	
			batman.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si4,valore4,entonces4,sino4));
			
			/*robin*/
			Caracteristica si5=new CaracteristicaFija((robin.getCaracteristica("Velocidad")));
	        Caracteristica valore5=new CaracteristicaFija(100);
	        Caracteristica entonces5=new CaracteristicaFija(batman.getCaracteristica("Velocidad"));
	        Caracteristica sino5=new CaracteristicaFija(1);	
			robin.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si5,valore5,entonces5,sino5));
			
			/*guason*/
		    Caracteristica si6=new CaracteristicaFija(guason.getCaracteristica("Velocidad"));
	        Caracteristica valore6=new CaracteristicaFija(100);
	        Caracteristica entonces6=new CaracteristicaFija(guason.getCaracteristica("Velocidad"));
	        Caracteristica sino6=new CaracteristicaFija(1);				
		 	guason.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si6,valore6,entonces6,sino6));
		 	
		 	/*lexluthor*/
		    Caracteristica si7=new CaracteristicaFija(lexluthor.getCaracteristica("Velocidad"));
            Caracteristica valore7=new CaracteristicaFija(100);
            Caracteristica entonces7=new CaracteristicaFija(lexluthor.getCaracteristica("Velocidad"));
            Caracteristica sino7=new CaracteristicaFija(1);		
			lexluthor.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si7,valore7,entonces7,sino7));
			
			/*gatubela*/
		    Caracteristica si8=new CaracteristicaFija(gatubela.getCaracteristica("Velocidad"));
            Caracteristica valore8=new CaracteristicaFija(100);
            Caracteristica entonces8=new CaracteristicaFija(gatubela.getCaracteristica("Velocidad"));
            Caracteristica sino8=new CaracteristicaFija(1);			
            gatubela.setCaracteristica("Resistencia al fuego", new CaracteristicaCondicional(si8,valore8,entonces8,sino8));
            
            System.out.println("==================Caracteristas de los Personajes==============[Resistencia al fuego] ");  System.out.println();
            System.out.println("_Caracterista Superman  Resistencia al fuego::    " +superman.getCaracteristica("Resistencia al fuego"));
            System.out.println("_Caracterista flash  Resistencia al fuego::       " +flash.getCaracteristica("Resistencia al fuego"));
            System.out.println("_Caracterista Capitanfrio  Resistencia al fuego:: " +capitanfrio.getCaracteristica("Resistencia al fuego"));
            System.out.println("_Caracterista Batman  Resistencia al fuego::      " +batman.getCaracteristica("Resistencia al fuego"));
            System.out.println("_Caracterista Robin  Resistencia al fuego::       " +robin.getCaracteristica("Resistencia al fuego"));
            System.out.println("_Caracterista Guason  Resistencia al fuego::      " +guason.getCaracteristica("Resistencia al fuego"));
            System.out.println("_Caracterista Lexluthor  Resistencia al fuego::   " +lexluthor.getCaracteristica("Resistencia al fuego"));
            System.out.println("_Caracterista Gatubela  Resistencia al fuego::    " +gatubela.getCaracteristica("Resistencia al fuego"));  System.out.println();
  
                                                  /*--Masa corporal--*/
            
        	superman.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",superman));
        	flash.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",flash));
        	capitanfrio.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",capitanfrio));
        	batman.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",batman));
        	robin.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",robin));
        	guason.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",guason));
        	lexluthor.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",lexluthor));
        	gatubela.setCaracteristica("Masa Corporal", new CaracteristicaDivision("Altura","Peso",gatubela));
        	
            System.out.println("==================Caracteristas de los Personajes================[Masa Corporal] ");   System.out.println();
            System.out.println("_Caracterista Superman  Masa Corporal::     " +superman.getCaracteristica("Masa Corporal"));
            System.out.println("_Caracterista Flash  Masa Corporal::        " +flash.getCaracteristica("Masa Corporal"));
            System.out.println("_Caracterista Capitanfrio  Masa Corporal::  " +capitanfrio.getCaracteristica("Masa Corporal"));
            System.out.println("_Caracterista Batman Masa Corporal::        " +batman.getCaracteristica("Masa Corporal"));
            System.out.println("_Caracterista Robin  Masa Corporal::        " +robin.getCaracteristica("Masa Corporal"));
            System.out.println("_Caracterista Guason  Masa Corporal::       " +guason.getCaracteristica("Masa Corporal"));
            System.out.println("_Caracterista Lexluthor  Masa Corporal::    " +lexluthor.getCaracteristica("Masa Corporal"));
	        System.out.println("_Caracterista Gatubela  Masa Corporal::     " +gatubela.getCaracteristica("Masa Corporal"));System.out.println();
	        
                                           
	                                    /*--Nivel de felicidad--*/
	              	         
        	/*flash*/
	        Caracteristica si1=new CaracteristicaFija(flash.getCaracteristica("Masa corporal"));
            Caracteristica valorr=new CaracteristicaFija(0.8);
            Caracteristica entonces1=new CaracteristicaFija(flash.getCaracteristica("Altura"));    
            Caracteristica sino1=new CaracteristicaFija(flash.getCaracteristica("Peso"));
            
	        guason.setCaracteristica("Masa corporal", new CaracteristicaFija(2));
            flash.setCaracteristica("Masa corporal", new CaracteristicaFija(3));
        
            guason.setCaracteristica("Nivel de felicidad", new CaracteristicaDivision("maldad ","Masa corporal",guason));
            flash.setCaracteristica("Nivel de felicidad", new CaracteristicaCondicional(si1,valorr,entonces1,sino1));
           
            System.out.println("     [======Nivel de felicidad=====[Guason y Flash] ");System.out.println();
            System.out.println("_Caracteristas de Guason  Nivel de Felicidad:: "+guason.getCaracteristica("Nivel de felicidad"));
            System.out.println("_Caracteristas de Flash  Nivel de Felicidad :: "+flash.getCaracteristica("Nivel de felicidad"));  System.out.println();   			
			
           //======================================= Crear Grupo==================================*/
					
			Liga duodinamico=new Liga("Duo Dinamico");
			duodinamico.agregarEntidadAbstracta(batman);
			duodinamico.agregarEntidadAbstracta(robin);
			Liga ligadelajusticia=new Liga("liga de la justicia");
			ligadelajusticia.agregarEntidadAbstracta(flash);
			ligadelajusticia.agregarEntidadAbstracta(superman);
			ligadelajusticia.agregarEntidadAbstracta(duodinamico);
			Liga ligadelainjusticia=new Liga("liga de la injusticia");
			ligadelainjusticia.agregarEntidadAbstracta(capitanfrio);
			ligadelainjusticia.agregarEntidadAbstracta(guason);
			ligadelainjusticia.agregarEntidadAbstracta(lexluthor);
			ligadelainjusticia.agregarEntidadAbstracta(gatubela);
			

								          /*-- Load Caracteristica a un grupo--*/
		
	        duodinamico.setCaracteristica("Fuerza", new CalculadorSuma("Fuerza",duodinamico));
	        duodinamico.setCaracteristica("Velocidad", new CalculadorMin(duodinamico,"Velocidad"));
	        duodinamico.setCaracteristica("inteligencia", new CalculadorMax(duodinamico,"inteligencia"));
	        duodinamico.setCaracteristica("PromedioFuerza", new CalculadorPromedio("Fuerza",duodinamico));
            System.out.println("[==================Caracteristas de los Grupos=============] ");  System.out.println();

	        System.out.println("            [Grupo Duo dinamico]");System.out.println();	
	        System.out.println("_Fuerza absoluta de Duo dinamico::              "+duodinamico.getCaracteristica("Fuerza"));
	        System.out.println("_Velocidad minima de  Duo dinamico::            "+duodinamico.getCaracteristica("Velocidad"));
	        System.out.println("_Inteligencia Maxima  de   Duo dinamico::       "+duodinamico.getCaracteristica("inteligencia"));
	        System.out.println("_PromedioFuerza  de  Duo dinamico::             "+duodinamico.getCaracteristica("PromedioFuerza"));  System.out.println();
			           
	        ligadelajusticia.setCaracteristica("Fuerza", new CalculadorPromedio("Fuerza",ligadelajusticia));
	        ligadelajusticia.setCaracteristica("Velocidad", new CalculadorMin(ligadelajusticia,"Velocidad"));
	        ligadelajusticia.setCaracteristica("inteligencia", new CalculadorMax(ligadelajusticia,"inteligencia"));
	        ligadelajusticia.setCaracteristica("Fuerza", new CalculadorPromedio("Fuerza",ligadelajusticia));
	       
	        System.out.println("           [Grupo Liga de la justicia]"); System.out.println();				
	        System.out.println("_Fuerza absoluta de Liga de la justicia::             "+ligadelajusticia.getCaracteristica("Fuerza"));
	        System.out.println("_Velocidad minima de  Liga de la justicia::           "+ligadelajusticia.getCaracteristica("Velocidad"));
	        System.out.println("_Inteligencia Maxima  de  Liga de la justici::        "+ligadelajusticia.getCaracteristica("inteligencia"));
	        System.out.println("_PromedioFuerza de Liga de la justicia::              "+ligadelajusticia.getCaracteristica("PromedioFuerza")); System.out.println();
	      
	        ligadelainjusticia.setCaracteristica("Fuerza", new CalculadorPromedio("Fuerza",ligadelainjusticia));
		    ligadelainjusticia.setCaracteristica("Velocidad", new CalculadorMin(ligadelainjusticia,"Velocidad"));
		    ligadelainjusticia.setCaracteristica("maldad", new CalculadorSuma("maldad",ligadelainjusticia));
		    ligadelainjusticia.setCaracteristica("Fuerza", new CalculadorPromedio("Fuerza",ligadelainjusticia));
		    
		    System.out.println("         [Grupo Liga de la Injusticia]"); System.out.println();			
		    System.out.println("_Fuerza absoluta de Liga de la Injusticia::             "+ligadelainjusticia.getCaracteristica("Fuerza"));
		    System.out.println("_Velocidad minima de  Liga de la Injusticia::           "+ligadelainjusticia.getCaracteristica("Velocidad"));
		    System.out.println("_Inteligencia Maxima  de  Liga de la Injusticia::       "+ligadelainjusticia.getCaracteristica("maldad"));
		    System.out.println("_PromedioFuerza  de Liga de la Injusticia::             "+ligadelainjusticia.getCaracteristica("PromedioFuerza"));	 System.out.println();
		    System.out.println("[======================Enfrentamiento=================================]"); System.out.println();
		  
		 
		    //============================================Enfrentamiento=========================================*/  
		    Juego game=new Juego();
		    
		    	     	//================= Pelea Flash y capitan frio ==========*/
		 
		    System.out.println("            [Flash Vs Capitan frio]");System.out.println();	
		    System.out.println("_FLASH::");
		    System.out.println("_Fuerza::    "+flash.getCaracteristica("Fuerza"));
	        System.out.println("_Velocidad:: "+flash.getCaracteristica("Velocidad"));
	  	    System.out.println("_Maldad::    "+flash.getCaracteristica("maldad")); System.out.println();	
	  	    System.out.println("_CAPITAN FRIO::");
	  	    System.out.println("_Fuerza::     "+capitanfrio.getCaracteristica("Fuerza"));
	  	    System.out.println("_Velocidad::  "+capitanfrio.getCaracteristica("Velocidad"));
	  	    System.out.println("_Maldad::     "+capitanfrio.getCaracteristica("maldad")); System.out.println();		
	  	   
	  	    Criterio  a11 = new CMayor("maldad",null,null);
	  	    Criterio a2 = new CMayor("Velocidad",a11,null);
	  	    Criterio mayor = new CMayor("Fuerza",a2,null);
	  	  
	  	    EntidadAbstracta personajeGanador = game.pelea(flash, capitanfrio, mayor);
	  	    if (personajeGanador != null) {
	  	    	System.out.println("_El Ganador es:::  "   + personajeGanador.getName());  }
	  	    else		
	  	        System.out.println("Hubo empate o la caracteristica no existe" );System.out.println();  
	  	        System.out.println("[=================================================================]");System.out.println();
	  	  
	  	    
	  	               //============== Pelea Duo dinamico Vs Guason===============*/

			System.out.println("            [Duo dinamico vs Guason]");	System.out.println();		
			System.out.println("_GUASON::");
			System.out.println("_Masa Corporal:: "+guason.getCaracteristica("Masa Corporal"));
			System.out.println("_inteligencia::   "+guason.getCaracteristica("inteligencia"));System.out.println();
			System.out.println("_DUO DINAMICO::");
			System.out.println("_Masa Corporal::  "+duodinamico.getCaracteristica("Masa Corporal"));
 			System.out.println("_inteligencia::   "+duodinamico.getCaracteristica("inteligencia"));System.out.println();
 			
 			Criterio a21 = new CMenor("Masa Corporal",null,null);
 			Criterio mayor1 = new CMayor("inteligencia",a21,null);
 			
 			EntidadAbstracta personajeGanador1 = game.pelea(duodinamico, guason, mayor1);	
 			if (personajeGanador1 != null) {
 				System.out.println("_El Ganador es::: "   + personajeGanador1.getName());}
 			else		
 				System.out.println("Hubo empate o la caracteristica no existe" );System.out.println();	
 			    System.out.println("[====================================================================]");System.out.println();		
		
 				  	//============Pelea Liga de la justicia Vs la liga de la injusticia========*/
 			
 			System.out.println("      [Liga de la justicia] Vs [la Liga de la injusticia]");System.out.println();	
 			System.out.println("_LIGA DE LA JUSTICIA::");
 			System.out.println("_Fuerza::       "+ligadelajusticia.getCaracteristica("Fuerza"));
 			System.out.println("_Velocidad ::   "+ligadelajusticia.getCaracteristica("Velocidad"));
 			System.out.println("_inteligencia:: "+ligadelajusticia.getCaracteristica("inteligencia"));	System.out.println();
 			System.out.println("_LIGA DE LA INJUSTICIA::");
 			System.out.println("_Fuerza::       "+ligadelainjusticia.getCaracteristica("Fuerza"));
 			System.out.println("_Velocidad::    "+ligadelainjusticia.getCaracteristica("Velocidad"));
 			System.out.println("_inteligencia:: "+ligadelainjusticia.getCaracteristica("inteligencia"));System.out.println();		
 			
 			Criterio  a111 = new CMayor("Fuerza",null,null);
 			Criterio a211 = new CMayor("Velocidad",a111,null);
 			Criterio mayor11 = new CMayor("inteligencia",a211,null);
 			EntidadAbstracta personajeGanador11 = game.pelea(ligadelajusticia, ligadelainjusticia, mayor11);
			
 			if (personajeGanador11 != null) {
 				System.out.println("_El Ganador es::: "   + personajeGanador11.getName());}
 			else		
 				System.out.println("Hubo empate o la caracteristica no existe" );System.out.println();
 			    System.out.println("[==============================Criterio=========================]");	System.out.println();	
 				
 			//========================Dado un criterio,Buscar vencedores===========================*/
 			
 			
 			                   //ADD ALL Personajes y Grupos//
 								game.addPersonaje(superman);
 								game.addPersonaje(robin);
 								game.addPersonaje(batman);
 								game.addPersonaje(duodinamico);
 								
 			System.out.println("_Todos los peleadores que ganan de acuerdo a un criterio::");System.out.println();	
 				          	
 			
 			System.out.println("_Altura::  1.88");System.out.println();
 			double altura=1.88;
 		    Vencedor criterio = new Vencedor("Altura",altura);
 			Vector<EntidadAbstracta> personajes = new Vector<EntidadAbstracta>();	
 			personajes = game.buscarVencedores(criterio);	
 			
 			System.out.println("_Cantidad de vencedores:: " + personajes.size());System.out.println();	
 			System.out.println("_Personajes/Grupos que ganan::: "); System.out.println();	
			for (EntidadAbstracta a:personajes){
			System.out.println("_"+ a.getName() );}	System.out.println();	

			//======================================= Listar Ordenanamente==================================*/
			
			//============== Descendente===============*/
			
			Criterio may = new CMayor("Velocidad",null,null);
			Vector<EntidadAbstracta> ordenados = new Vector<EntidadAbstracta>();
			ordenados = game.listarOrdenado(may);
			    System.out.println("[==============================Ordenar=========================]");	System.out.println();	
			System.out.println("_Personajes/Grupos ordenados descendentemente por Velocidad::" );System.out.println();	
			for (int i = 0; i < ordenados.size(); i++) {
			System.out.println("_"+	ordenados.elementAt(i).getName());}	System.out.println();	
			
			 //============== Ascendente===============*/	
			
			Criterio mayot = new CMenor("Fuerza",null,null);
		
			Criterio mayo = new CMayor("maldad",mayot,null);
		
			Vector<EntidadAbstracta> ordenados1 = new Vector<EntidadAbstracta>();
			ordenados1 = game.listarOrdenado(mayo);
			
			System.out.println("_Personajes/Grupos ordenados ascendentemente por Maldad y por Fuerza::" );System.out.println();	
			for (int i1 = 0; i1 < ordenados1.size(); i1++) {
			System.out.println("_"+ordenados1.elementAt(i1).getName());	}
			}
		}

			
		
	
		

