package Criterio;

import java.util.Comparator;

import EntidadAbstracta.EntidadAbstracta;

public abstract class Criterio implements Comparator<EntidadAbstracta> {
     protected Criterio sig;
	 protected String carac;
    protected Object valor;

	public Criterio (String carac,Object valor, Criterio sig ) 
	{ this.carac = carac; this.sig = sig; }
	

	public abstract int compare(EntidadAbstracta e1,EntidadAbstracta e2) ;
	

}







































