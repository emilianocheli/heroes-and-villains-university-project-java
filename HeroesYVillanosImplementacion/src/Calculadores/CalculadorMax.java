package Calculadores;

import java.util.LinkedList;
import java.util.List;

import EntidadAbstracta.EntidadAbstracta;
import EntidadAbstracta.Personaje;

public class CalculadorMax implements Calculador {
	private EntidadAbstracta entidad;
	private String caract;
	public CalculadorMax(EntidadAbstracta grupo,String c){
		this.entidad = grupo;
		this.caract=c;
	}
	public double getValor() {
		List<Personaje> personajes=new LinkedList<Personaje>();
        
		entidad.getPersonajes(personajes);
		double max=0.0;
		for (Personaje personaje:personajes)
		
			if (personaje.getCaracteristica(caract)>max)
				max = personaje.getCaracteristica(caract);
			
		
		return max;
    	
    }
}
