package Calculadores;

import java.util.LinkedList;
import java.util.List;

import EntidadAbstracta.EntidadAbstracta;
import EntidadAbstracta.Personaje;

	public class CalculadorMin implements Calculador {
		private EntidadAbstracta entidad;
		private String caract;
		public CalculadorMin(EntidadAbstracta grupo,String c){
			this.entidad = grupo;
			this.caract=c;
		}
		public double getValor() {
			List<Personaje> personajes=new LinkedList<Personaje>();
	        
			entidad.getPersonajes(personajes);
			double min= 99999999;
			for (Personaje personaje:personajes)
			
				if (personaje.getCaracteristica(caract)<min)
					min = personaje.getCaracteristica(caract);
				
			
			return min;
			
	    	
	    }
	}


