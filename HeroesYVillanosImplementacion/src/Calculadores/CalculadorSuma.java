package Calculadores;

import java.util.LinkedList;
import java.util.List;

import EntidadAbstracta.EntidadAbstracta;

import EntidadAbstracta.Personaje;

public class CalculadorSuma implements Calculador{
	
	private String caract;
	protected EntidadAbstracta entidad;
	
	public CalculadorSuma(String caract, EntidadAbstracta entidad) {
		this.caract=caract;
		this.entidad=entidad;
	}

	public double getValor() {
		List<Personaje> personajes=new LinkedList<Personaje>();
        entidad.getPersonajes(personajes);
		double suma=0.0;
		for (Personaje personaje:personajes) {
			suma+=personaje.getCaracteristica(caract);
		}
		return suma;
}
}