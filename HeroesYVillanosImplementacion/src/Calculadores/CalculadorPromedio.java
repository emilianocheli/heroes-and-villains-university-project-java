package Calculadores;

import java.util.LinkedList;
import java.util.List;

import EntidadAbstracta.EntidadAbstracta;
import EntidadAbstracta.Personaje;

public class CalculadorPromedio extends CalculadorSuma {
			
		public CalculadorPromedio(String caract, EntidadAbstracta entidad) {
		super(caract, entidad);
	
	}		
		public double getValor() {
			List<Personaje> personajes=new LinkedList<Personaje>();
		    entidad.getPersonajes(personajes);
			double promedio=0.0;
			promedio=super.getValor()/personajes.size();
		
	           return promedio;
		}
}

