package Caracteristica;

public class CaracteristicaCondicional  implements Caracteristica{
	
	private Caracteristica primero;
	private Caracteristica segundo ;
	private Caracteristica verdadero;
	private Caracteristica falso ; 
	public CaracteristicaCondicional(Caracteristica a1, Caracteristica a2, Caracteristica a3, Caracteristica a4) {
		this.primero = a1;
		this.segundo = a2;
		this.verdadero = a3;
		this.falso = a4;
	
	}
	public double getValor() {
		if (primero.getValor() > segundo.getValor())
			return verdadero.getValor();
		else
			return falso.getValor();
	}

}



