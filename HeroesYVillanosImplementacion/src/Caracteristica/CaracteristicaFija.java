package Caracteristica;

public class CaracteristicaFija implements Caracteristica{
	
	private double valor;
	
	public CaracteristicaFija(double valor){
		this.valor=valor;
	}
	
	public double getValor() {
		return valor;
	}

}
