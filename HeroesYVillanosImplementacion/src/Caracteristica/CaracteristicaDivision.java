package Caracteristica;

import EntidadAbstracta.Personaje;

public class CaracteristicaDivision  implements Caracteristica{

	private String carac1;
	private String carac2;
	private Personaje p;
	public CaracteristicaDivision (String carac1,String carac2,Personaje p){
		
	this.carac1=carac1;
	this.carac2=carac2;
	this.p=p;	
	}
	public double getValor() {
		double resultado=0.0;
		resultado= p.getCaracteristica(carac1)/p.getCaracteristica(carac2);
		
		return resultado ;
	}

}