package EntidadAbstracta;
import java.util.List;

    public abstract class EntidadAbstracta {
    private String nombre;
	public EntidadAbstracta(String n) {
			this.nombre=n;
		}
	public abstract void getPersonajes(List<Personaje> personajes);
	public abstract double getCaracteristica(String caracteristica) ;
	public String getName() {
		return nombre;
	}
}