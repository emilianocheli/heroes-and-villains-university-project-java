package EntidadAbstracta;

import java.util.Hashtable;
import java.util.List;

import Caracteristica.Caracteristica;

public class Personaje extends EntidadAbstracta{

private  Hashtable<String,Caracteristica> personajes;
private String nombreReal;

//Constructor Personaje

public Personaje(String n){
	super(n);
	personajes=new Hashtable<String,Caracteristica>(); 
}
//Metodos Personaje

public void setCaracteristica(String caracteristica,Caracteristica caract){
	personajes.put(caracteristica, caract);
}

public double getCaracteristica(String caracteristica){
	if (!personajes.containsKey(caracteristica))
		return 0;
	else    
		 return  personajes.get(caracteristica).getValor();
};
public String getNamesupereal(){
	return  nombreReal;
}
public void addNombreReal(String n){
	this.nombreReal=n;
}
public void getPersonajes(List<Personaje> personajes) {
		personajes.add(this);
	}


}
