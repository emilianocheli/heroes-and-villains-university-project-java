package EntidadAbstracta;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import Calculadores.Calculador;
public class Liga extends EntidadAbstracta{
	private Vector<EntidadAbstracta> grupo;
	private HashMap<String,Calculador>OperacionGrupo;
	
	//Contructor De la liga
	public Liga(String n  ){	
	   super(n);
		grupo=new Vector<EntidadAbstracta>();
		OperacionGrupo=new HashMap<String,Calculador>();
	}
	
	public void agregarEntidadAbstracta(EntidadAbstracta nuevaEntidad){
		grupo.add(nuevaEntidad);
	}

	@Override
	public void getPersonajes(List<Personaje> personajes) {
		for (EntidadAbstracta entidad : grupo) {
			entidad.getPersonajes(personajes);
		}
	}		
	public void setCaracteristica(String caracteristica,Calculador operacion){
		OperacionGrupo.put(caracteristica, operacion);
	};
	
	public  double getCaracteristica(String caracteristica){
		if (OperacionGrupo.containsKey(caracteristica)) 
		return OperacionGrupo.get(caracteristica).getValor();
		return 0;
		
	} ;
	
}
